
## Desafio de capacitação em NodeJS

[Link](https://gitlab.com/acens/capacitacens/back-end/desafio-02-atualizado) para o desafio original. Fiz algumas modificações mínimas como correção de algumas palavras para coincidir com inglês (CEP -> zip_code), zip_code como string caso o CEP possua '0' no início (nem sei se é possível) e id de 'recipient' como auto-increment.

### **Token jwt**

Testado com [insomnia](https://insomnia.rest/) o header deve ficar da seguinte forma:

    Autorization
    Bearer eUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1h0.Zq6SXPYChKlVK-uoXD

Todas as rotas exceto `POST /login` exigem um token. As ações realizadas em `recipient` não exigem um usuário admin.

### **Inputs**

### POST /dashboard/users
`
	{
		"name": "Distribuidora FastSticker",
		"email": "admin@faststicker.com"
	}
`

exige conta **admin**

### POST /dashboard/recipient

`
	{
		"name": "Vinicius Eduardo",
		"address": "Rua treze",
		"number": 445,
		"complement": "Altos",
		"estate": "Disney",
		"city": "Nárnia",
		"zip_code": "0567890123"
	}
`

**Não**  exige conta admin

### GET /dashboard/users

Por conta da paginação a rota para fica da seguinte forma:

    /dashboard/users?page=1

exige conta **admin**


Feito com ♥ by AcensJr :wave:
