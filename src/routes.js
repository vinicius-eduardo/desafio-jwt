const express = require('express');
const authToken = require('./app/middlewares/authToken');

const routes = express.Router();

const userController = require('./app/controllers/UserController');
const recipientController = require('./app/controllers/RecipientController');
const loginController = require('./app/controllers/LoginController');
const myProfileController = require('./app/controllers/MyProfileController');

routes.post('/login', loginController.login);

routes.use(authToken);

routes.get('/dashboard/users', userController.index);
routes.post('/dashboard/users', userController.store);

routes.get('/dashboard/my-profile', myProfileController.searchFor);
routes.delete('/my-profile', myProfileController.destroy);
routes.put('/dashboard/my-profile', myProfileController.updateUser);

routes.post('/dashboard/recipient', recipientController.store);
routes.get('/dashboard/recipient', recipientController.index);
routes.get('/dashboard/recipient/:id', recipientController.searchFor);
routes.put('/dashboard/recipient/:id', recipientController.updateRecipient);
routes.delete('/dashboard/recipient/:id', recipientController.destroy);

module.exports = routes;
