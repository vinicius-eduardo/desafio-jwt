const Sequelize = require('sequelize');
const dbConfig = require('../config/database.js');
const Recipient = require('../app/models/Recipient');
const User = require('../app/models/User');

const connection = new Sequelize(dbConfig);

User.init(connection);
Recipient.init(connection);

module.exports = connection;
