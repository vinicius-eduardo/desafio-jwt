const Yup = require('yup');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const authPassword = require('../middlewares/authPassword');

module.exports = {
  async searchFor(req, res) {
    global.console.log(req.user.id);
    const user = await User.findByPk(req.user.id, {
      attributes: ['id', 'admin', 'name', 'email'],
    });

    if (!user) {
      res.status(404).send('Usuário não encontrado.');
    }

    return res.json(user);
  },
  async destroy(req, res) {
    const result = await User.destroy({
      where: { id: req.user },
    });

    if (result) {
      return res.send('destruido');
    }
    return res.send('não destruido');
  },
  async updateUser(req, res) {
    // requires password confirmation only if the old password is entered.
    let param = {
      name: Yup.string(),
      email: Yup.string().email(),
    };

    if (req.body.oldPassword) {
      param = {
        ...param,
        password: Yup.string().required(),
        confirmPassword: Yup.string()
          .oneOf([Yup.ref('password')])
          .required(),
      };
    }

    const schema = Yup.object().shape(param);

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const user = await User.findByPk(req.user.id);

    if (req.body.email && req.body.email !== user.email) {
      const invalidMail = await User.findOne({
        where: { email: req.body.email },
      });

      if (invalidMail) {
        return res.status(400).send('Email já foi utilizado.');
      }
    }

    let newUser = req.body;

    if (req.body.oldPassword) {
      if (authPassword.check(req.body.oldPassword, user.password_hash)) {
        if (authPassword.check(req.body.password, user.password_hash)) {
          return res.status(400)
            .send('Sua senha deve ser diferente da senha antiga');
        }
      } else {
        return res
          .status(400)
          .send('Senha atual incorreta. (oldPassword)');
      }

      const password_hash = await bcrypt.hash(req.body.password, 10);

      newUser = { ...newUser, password_hash };
    }

    const result = await User.update(newUser, {
      where: { id: req.user.id },
    });

    if (result) {
      return res.status(200).send('Dados atualizados');
    }
    return res.status(400).send('Os dados não foram atualizados');
  },
};
