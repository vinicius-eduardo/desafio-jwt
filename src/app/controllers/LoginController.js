const jwt = require('jsonwebtoken');
const authPassword = require('../middlewares/authPassword');
const User = require('../models/User');

module.exports = {
  async login(req, res) {
    const user = await User.findOne({
      where: { email: req.body.email },
    });

    if (!user) {
      res.status(404).send('Usuário não encontrado.');
    }

    if (authPassword.check(req.body.password, user.password_hash)) {
      const token = jwt.sign({
        id: user.id,
        admin: user.admin,
      }, process.env.SECRET, { expiresIn: '10m' });
      res.json({
        id: user.id,
        name: user.name,
        email: user.email,
        token,
      });
    } else {
      res.send('Password errado');
    }
  },
};
