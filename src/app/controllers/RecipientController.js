const Yup = require('yup');
require('dotenv/config');
const Recipient = require('../models/Recipient');

module.exports = {
  async index(req, res) {
    const {
      page,
    } = req.query;
    const limit = 10;
    const offset = limit * (page - 1);

    const recipients = await Recipient.findAll({
      order: ['id'],
      limit,
      offset,
    });

    if (recipients.length === 0) {
      if (offset > 0) {
        return res.status(404).send('Você chegou ao fim da lista');
      }
      return res.status(404).send('Ainda não existe destinatários');
    }

    return res.json(recipients);
  },
  async searchFor(req, res) {
    const recipient = await Recipient.findByPk(req.params.id, {
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
    });

    if (!recipient) {
      res.status(404).send('Destinatário não encontrado.');
    }

    return res.json(recipient);
  },
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      address: Yup.string().required(),
      number: Yup.number().integer().required(),
      complement: Yup.string().required(),
      estate: Yup.string().required(),
      city: Yup.string().required(),
      zip_code: Yup.string().required(),
    });

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const result = await Recipient.create(req.body);
    if (result) {
      return res.status(200).send(result);
    }
    return res.status(400);
  },
  async destroy(req, res) {
    const result = await Recipient.destroy({
      where: {
        id: req.params.id,
      },
    });

    global.console.log(result);

    if (result) {
      return res.send('destruido');
    }
    return res.status(404).send('não destruido');
  },
  async updateRecipient(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      address: Yup.string(),
      number: Yup.number().integer(),
      complement: Yup.string(),
      estate: Yup.string(),
      city: Yup.string(),
      zip_code: Yup.string(),
    });

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const recipient = await Recipient.findByPk(req.params.id);

    if (!recipient) {
      res.status(404).send('Destinatário não encontrado.');
    }

    const result = await Recipient.update(req.body, {
      where: {
        id: req.params.id,
      },
    });

    if (result) {
      return res.status(200).send('Dados atualizados');
    }
    return res.status(400).send('Os dados não foram atualizados');
  },
};
