const Yup = require('yup');
const bcrypt = require('bcryptjs');
require('dotenv/config');
const User = require('../models/User');

module.exports = {
  async index(req, res) {
    if (!req.user.admin) return res.status(401).send('Sua conta não tem autorização para isso.');

    const { page } = req.query;
    const limit = 10;
    const offset = limit * (page - 1);

    const users = await User.findAll({
      attributes: ['admin', 'id', 'name', 'email'],
      order: ['id'],
      limit,
      offset,
    });

    if (users.length === 0) {
      if (offset > 0) {
        return res.status(404).send('Você chegou ao fim da lista');
      }
      return res.status(404).send('Ainda não existe usuários');
    }

    return res.json(users);
  },
  async store(req, res) {
    if (!req.user.admin) return res.status(401).send('Sua conta não tem autorização para isso.');

    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required(),
      admin: Yup.boolean().required(),
      confirmPassword: Yup.string().oneOf([Yup.ref('password')]).required(),
    });

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const findUser = await User.findOne({
      where: { email: req.body.email },
    });

    if (findUser) {
      res.status(400).send('Este email já foi cadastrado');
    }

    const password_hash = await bcrypt.hash(req.body.password, 10);

    const newUser = { ...req.body, password_hash };

    const result = await User.create(newUser);

    if (result) {
      return res.status(200).json({
        id: result.id,
        admin: result.admin,
        name: result.name,
        email: result.email,
      });
    }
    return res.status(400);
  },
};
