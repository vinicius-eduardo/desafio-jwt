const {
  Model,
  DataTypes,
} = require('sequelize');

class User extends Model {
  static init(conenction) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      admin: DataTypes.BOOLEAN,
      password_hash: DataTypes.STRING,
    }, {
      sequelize: conenction,
    });
  }
}

module.exports = User;
