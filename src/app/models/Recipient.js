const {
  Model,
  DataTypes,
} = require('sequelize');

class Recipient extends Model {
  static init(conenction) {
    super.init({
      name: DataTypes.STRING,
      address: DataTypes.STRING,
      number: DataTypes.INTEGER,
      complement: DataTypes.STRING,
      estate: DataTypes.STRING,
      city: DataTypes.STRING,
      zip_code: DataTypes.STRING,
    }, {
      sequelize: conenction,
    });
  }
}

module.exports = Recipient;
