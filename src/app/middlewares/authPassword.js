const bcrypt = require('bcryptjs');

module.exports = {
  check(password, hash) {
    return bcrypt.compareSync(password, hash);
  },
};
